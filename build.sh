#!/bin/sh

printrun() {
    echo "$@"
    "$@"
}

CC="${CC:-gcc}"

flags=""
flags="$flags "
flags="$flags -Og -ggdb3"
flags="$flags -std=gnu11"
flags="$flags -Wall -Wextra -Werror -Wno-unused-function -Werror=return-type -Werror=type-limits"
flags="$flags -Wimplicit-fallthrough -Wuninitialized -Wignored-attributes -Winit-self -Wshadow -Wundef"
flags="$flags -Wfloat-conversion -Wsign-conversion -Wconversion -Wwrite-strings -Werror=implicit-function-declaration"
flags="$flags -Wunused-result -Wnarrowing -Wmissing-field-initializers"

# flags="$flags -m32"
flags="$flags -m64"

printrun $CC $flags tapearchiver.c -o tapearchiver
