// See man 5 tar
//
// NOTE: Numeric fields: 
//  Option A: Padded with zeroes or spaces.
//  Option B: First byte has the high bit set. The following bytes represent a two's complement signed integer (TODO I guess it is big endian but it didn't specify).

#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>



#define JOIN2_A(a, b) a##b
#define JOIN2(a, b) JOIN2_A(a, b)
#define BREAKPOINT() do{asm volatile ("int3"); int volatile __attribute__((unused)) __breakpoint123 = 123;}while(0)
#define ARRAY_COUNT(x) (sizeof((x))/sizeof((x)[0]))
#define STATIC_ASSERT(cond) static char __attribute__((unused)) JOIN2(___static_assert__, __COUNTER__)[cond ? 0 : -1]
#define ASSERT(cond) do{if (!(cond)) { BREAKPOINT(); abort(); }}while(0)
#define MINIMUM(a, b) ((a) < (b) ? (a) : (b))
#define MAXIMUM(a, b) ((a) > (b) ? (a) : (b))

#define TAR_RECORD_SIZE_IN_BYTES 512

#define GNU_TAR_MAGIC_PTR "ustar "
#define GNU_TAR_MAGIC_LEN 6
struct gnu_tar_header {
  char pathname[100];
  char mode[8];
  char uid[8];
  char gid[8];
  char size[12];
  char mtime[12];
  char checksum[8];
  char typeflag;
  char linkname[100];
  char magic[6];
  char version[2];
  char uname[32];
  char gname[32];
  char devmajor[8];
  char devminor[8];
  char atime[12];
  char ctime[12];
  char offset[12];
  char longnames[4];
  char _unused[1];
  struct {
    char offset[12];
    char numbytes[12];
  } sparse[4];
  // If `isextended` is non-zero, `struct gnu_tar_header` will be followed by additional `gnu_sparse_header` records.
  char isextended;
  char realsize[12];
  char _pad[17];
};
STATIC_ASSERT(sizeof(struct gnu_tar_header) == TAR_RECORD_SIZE_IN_BYTES);

struct gnu_sparse_header {
  struct {
    char offset[12];
    char numbytes[12];
  } sparse[21];
  // I guess `isextended` has the same meaning as in `struct gnu_tar_header`.
  char isextended;
  char padding[7];
};
STATIC_ASSERT(sizeof(struct gnu_sparse_header) == TAR_RECORD_SIZE_IN_BYTES);

static size_t my_strnlen(char const *ptr, size_t max_len) {
  size_t i = 0;
  while (i < max_len && ptr[i] != 0) {
    i += 1;
  }
  return i;
}

/// Read at most `records_to_read` into `buf`. Returns the number of records read. A return value of zero means EOF.
size_t read_records(char *buf, size_t records_to_read, FILE *stream) {
  return fread(buf, TAR_RECORD_SIZE_IN_BYTES, records_to_read, stream);
}

int skip_records(uint64_t records_to_skip, FILE *stream) {
  // 0 = success, else(-1) failure
  // TODO: 64 bit seek on 32bit target
  return fseek(stream, (long)records_to_skip * TAR_RECORD_SIZE_IN_BYTES, SEEK_CUR);
}

int parse_octal_integer32(uint32_t *ret, char const *buf_ptr, size_t buf_len) {
  bool got_digit = false;
  uint32_t acc = 0;
  // TODO: Error checking.
  for (size_t i = 0; i < buf_len; i += 1) {
    switch (buf_ptr[i]) {
    case '0': case '1': case '2': case '3':
    case '4': case '5': case '6': case '7': {
      got_digit = true;
      acc *= 8;
      acc += (uint32_t)(buf_ptr[i] - '0');
    } break;
    case ' ': {
      if (got_digit) {
        goto outer;
      }
    } break;
    case 0: { goto outer; } break;
    default: {} break;
    }
  }
  outer:;
  *ret = acc;
  return 0;
}
int parse_octal_integer64(uint64_t *ret, char const *buf_ptr, size_t buf_len) {
  bool got_digit = false;
  uint64_t acc = 0;
  // TODO: Error checking.
  for (size_t i = 0; i < buf_len; i += 1) {
    switch (buf_ptr[i]) {
    case '0': case '1': case '2': case '3':
    case '4': case '5': case '6': case '7': {
      got_digit = true;
      acc *= 8;
      acc += (uint64_t)(buf_ptr[i] - '0');
    } break;
    case ' ': {
      if (got_digit) {
        goto outer;
      }
    } break;
    case 0: { goto outer; } break;
    default: {} break;
    }
  }
  outer:;
  *ret = acc;
  return 0;
}

/// A Dynamic array of `char`.
typedef struct {
  char *ptr;
  size_t len;
  size_t capacity;
} StringDynamic;

void string_dynamic_init(StringDynamic *str) {
  *str = (StringDynamic){.ptr = NULL, .len = 0, .capacity = 0};
}
void string_dynamic_deinit(StringDynamic *str) {
  if (str->ptr) free(str->ptr);
  *str = (StringDynamic){.ptr = NULL, .len = 0, .capacity = 0};
}
void string_dynamic_required_unused_capacity(StringDynamic *str, size_t unused_capacity_required) {
  if (str->capacity - str->len < unused_capacity_required) {
    size_t new_capacity = MAXIMUM(str->len + unused_capacity_required, str->capacity * 2);
    void *new_ptr = realloc(str->ptr, new_capacity*sizeof(char));
    ASSERT(new_ptr);
    str->ptr = new_ptr;
    str->capacity = new_capacity;
  }
}
void string_dynamic_init_capacity(StringDynamic *str, size_t cap) {
  string_dynamic_init(str);
  string_dynamic_required_unused_capacity(str, cap);
}
void string_dynamic_append_ptr_len(StringDynamic *str, char *ptr, size_t len) {
  string_dynamic_required_unused_capacity(str, len);
  memcpy(&str->ptr[str->len], ptr, len);
  str->len += len;
}

enum entry_type {
  E_entry_type__unknown,
  E_entry_type__regular_file_0,
  E_entry_type__directory_0,
  E_entry_type__hard_link_1,
  E_entry_type__symbolic_link_2,
  E_entry_type__directory_5,
  E_entry_type__gnu_D,
  E_entry_type__gnu_K,
  E_entry_type__gnu_L,
};

void print_tar_entry(enum entry_type ty, uint64_t contents_size, char const *pathname_ptr, uint32_t pathname_len, char const *linkname_ptr, uint32_t linkname_len, uint32_t mode) {
  bool is_dir = ty == E_entry_type__directory_0 || ty == E_entry_type__directory_5;
  unsigned is_link = (ty == E_entry_type__symbolic_link_2) ? 2 : ((ty == E_entry_type__hard_link_1) ? 1 : 0);
  char mode_buf[10+1];
  mode_buf[0] = is_dir ? 'd' : (is_link == 2 ? 'l' : (is_link == 1 ? 'h' : '-'));
  mode_buf[1] = (mode & (1 << 8)) != 0 ? 'r' : '-';
  mode_buf[2] = (mode & (1 << 7)) != 0 ? 'w' : '-';
  mode_buf[3] = (mode & (1 << 6)) != 0 ? 'x' : '-';
  mode_buf[4] = (mode & (1 << 5)) != 0 ? 'r' : '-';
  mode_buf[5] = (mode & (1 << 4)) != 0 ? 'w' : '-';
  mode_buf[6] = (mode & (1 << 3)) != 0 ? 'x' : '-';
  mode_buf[7] = (mode & (1 << 2)) != 0 ? 'r' : '-';
  mode_buf[8] = (mode & (1 << 1)) != 0 ? 'w' : '-';
  mode_buf[9] = (mode & (1 << 0)) != 0 ? 'x' : '-';
  mode_buf[10] = 0;
  char const date_buf[] = "todo-date";
  uint32_t const date_len = ARRAY_COUNT(date_buf)-1;
  if (is_link == 0) {
    printf("%10s uid gid %16"PRIu64" %.*s %.*s\n", mode_buf, contents_size, date_len, date_buf, pathname_len, pathname_ptr);
  } else {
    printf("%10s uid gid %16"PRIu64" %.*s %.*s -> (%s) %.*s\n",
           mode_buf, contents_size, date_len, date_buf,
           pathname_len, pathname_ptr,
           ((is_link == 2) ? "symlink" : "hardlink"),
           linkname_len, linkname_ptr
    );
  }
}

int main(int argc, char **argv) {
  if (argc != 2) {
    char const *argv0 = argc > 0 ? argv[0] : "tapearchiver";
    printf("Usage: %s <filename.tar>\n", argv0);
    return 1;
  }
  char const *filename = argv[1];
  FILE *stream = fopen(filename, "rb");
  if (!stream) {
    printf("Fail fopen filename '%s'\n", filename);
    return 1;
  }

  static char const zero_record_bytes[TAR_RECORD_SIZE_IN_BYTES] = {0};
  bool last_record_was_zero = false;
  bool found_two_zero_records = false;
  bool has_big_linkname = false;
  bool has_big_pathname = false;
  StringDynamic str_dyn_pathname;
  string_dynamic_init_capacity(&str_dyn_pathname, TAR_RECORD_SIZE_IN_BYTES);
  StringDynamic str_dyn_linkname;
  string_dynamic_init_capacity(&str_dyn_linkname, TAR_RECORD_SIZE_IN_BYTES);
  while (1) {
    char buf1[TAR_RECORD_SIZE_IN_BYTES];
    if (read_records(buf1, 1, stream) != 1) {
      found_two_zero_records = false;
      break;
    }
    if (memcmp(zero_record_bytes, buf1, TAR_RECORD_SIZE_IN_BYTES) == 0) {
      if (last_record_was_zero) {
        found_two_zero_records = true;
        break;
      }
      last_record_was_zero = true;
      continue;
    } else {
      if (last_record_was_zero) {
        // TODO: What do I do?
        BREAKPOINT();
      }
    }

    struct gnu_tar_header *header = (struct gnu_tar_header *)buf1;
    if (memcmp(GNU_TAR_MAGIC_PTR, header->magic, ARRAY_COUNT(header->magic)) != 0) {
      BREAKPOINT();
      printf("Bad gnutar magic from '%s'\n", filename);
      return 1;
    }
    uint32_t small_linkname_len = (uint32_t)my_strnlen(header->linkname, ARRAY_COUNT(header->linkname));
    uint32_t small_pathname_len = (uint32_t)my_strnlen(header->pathname, ARRAY_COUNT(header->pathname));
    bool pathname_ends_with_slash;
    if (has_big_pathname) {
      pathname_ends_with_slash = str_dyn_pathname.ptr[str_dyn_pathname.len-1] == '/';
    } else {
      pathname_ends_with_slash = header->pathname[small_pathname_len-1] == '/';
    }


    enum entry_type entry_type = E_entry_type__unknown;
    switch (header->typeflag) {
    case 0: case '0': {
      // regular file or directory
      entry_type = pathname_ends_with_slash ? E_entry_type__directory_0 : E_entry_type__regular_file_0;
    } break;
    case '1': {
      entry_type = E_entry_type__hard_link_1;
    } break;
    case '2': {
      entry_type = E_entry_type__symbolic_link_2;
    } break;
    case '5': {
      entry_type = E_entry_type__directory_5;
    } break;
    case 'D': {
      entry_type = E_entry_type__gnu_D;
    } break;
    case 'K': {
      // the data for this entry is a long linkname for the following regular entry
      entry_type = E_entry_type__gnu_K;
    } break;
    case 'L': {
      // the data for this entry is a long pathname for the following regular entry
      entry_type = E_entry_type__gnu_L;
    } break;
    default: {} break;
    }

    switch (entry_type) {
    case E_entry_type__unknown: {
      BREAKPOINT();
      has_big_pathname = false;
      has_big_linkname = false;
    } break;
    case E_entry_type__regular_file_0: {
      has_big_linkname = false;
      char const *pathname_ptr;
      uint32_t pathname_len;
      if (has_big_pathname) {
        has_big_pathname = false;
        pathname_ptr = str_dyn_pathname.ptr;
        pathname_len = (uint32_t)str_dyn_pathname.len;
      } else {
        pathname_ptr = header->pathname;
        pathname_len = small_pathname_len;
      }
      if (header->isextended) {
        // TODO
        BREAKPOINT();
      }
      // Example ls -lhaF:
      // drwxr-xr-x  3 xzto users 4.0K Jan 14 16:24 ./
      // drwxr-xr-x 65 xzto users 4.0K Jan 14 12:52 ../
      // -rwxr-xr-x  1 xzto users  590 Jan 14 13:37 build.sh*
      // -rw-r--r--  1 xzto users   54 Jan 14 13:44 .editorconfig
      // -rw-r--r--  1 xzto users  13K Jan 14 15:20 mymacro_common.c
      // -rw-r--r--  1 xzto users 247M Jan 14 13:54 riscv64--musl--bleeding-edge-2020.08-1.tar
      // Example tar tvf test.tar
      // -rw-r--r-- xzto/users     5007 2020-11-14 22:54 pak.zig


      // Skip the file contents
      {
        // Get file size. header->size is 12 bytes in base 8, which is equal to 36bits in base2.
        uint64_t contents_size;
        if (parse_octal_integer64(&contents_size, header->size, ARRAY_COUNT(header->size)) != 0) {
          fprintf(stderr, "Bad file size '%.*s'\n", pathname_len, pathname_ptr);
          return 1;
        }
        // Get file mode/permissions (rwxrwxrwx)
        uint32_t mode;
        if (parse_octal_integer32(&mode, header->mode, ARRAY_COUNT(header->mode)) != 0) {
          fprintf(stderr, "Bad file mode '%.*s'\n", pathname_len, pathname_ptr);
          return 1;
        }
        print_tar_entry(entry_type, contents_size, pathname_ptr, pathname_len, 0, 0, mode);
        // TODO: Make sure that empty files do not have one extra record.
        uint64_t records_for_contents = (contents_size + TAR_RECORD_SIZE_IN_BYTES - 1) / TAR_RECORD_SIZE_IN_BYTES;
        if (skip_records(records_for_contents, stream) != 0) {
          printf("File is truncated\n");
          return 1;
        }
      }
    } break;
    case E_entry_type__directory_0: {
      has_big_linkname = false;
      char const *pathname_ptr;
      uint32_t pathname_len;
      if (has_big_pathname) {
        has_big_pathname = false;
        pathname_ptr = str_dyn_pathname.ptr;
        pathname_len = (uint32_t)str_dyn_pathname.len;
      } else {
        pathname_ptr = header->pathname;
        pathname_len = small_pathname_len;
      }
      if (header->isextended) {
        // TODO
        BREAKPOINT();
      }
      {
        // Get file mode/permissions (rwxrwxrwx)
        uint32_t mode;
        if (parse_octal_integer32(&mode, header->mode, ARRAY_COUNT(header->mode)) != 0) {
          fprintf(stderr, "Bad file mode '%.*s'\n", pathname_len, pathname_ptr);
          return 1;
        }
        print_tar_entry(entry_type, 0, pathname_ptr, pathname_len, 0, 0, mode);
      }
    } break;
    case E_entry_type__directory_5: {
      has_big_linkname = false;
      char const *pathname_ptr;
      uint32_t pathname_len;
      if (has_big_pathname) {
        has_big_pathname = false;
        pathname_ptr = str_dyn_pathname.ptr;
        pathname_len = (uint32_t)str_dyn_pathname.len;
      } else {
        pathname_ptr = header->pathname;
        pathname_len = small_pathname_len;
      }
      if (header->isextended) {
        // TODO
        BREAKPOINT();
      }
      {
        // Get file mode/permissions (rwxrwxrwx)
        uint32_t mode;
        if (parse_octal_integer32(&mode, header->mode, ARRAY_COUNT(header->mode)) != 0) {
          fprintf(stderr, "Bad file mode '%.*s'\n", pathname_len, pathname_ptr);
          return 1;
        }
        print_tar_entry(entry_type, 0, pathname_ptr, pathname_len, 0, 0, mode);
      }
    } break;
    case E_entry_type__gnu_D: {
      printf("Got gnu_D\n");
      BREAKPOINT();
    } break;
    case E_entry_type__symbolic_link_2: case E_entry_type__hard_link_1: {
      char const *pathname_ptr;
      uint32_t pathname_len;
      if (has_big_pathname) {
        has_big_pathname = false;
        pathname_ptr = str_dyn_pathname.ptr;
        pathname_len = (uint32_t)str_dyn_pathname.len;
      } else {
        pathname_ptr = header->pathname;
        pathname_len = small_pathname_len;
      }
      char const *linkname_ptr;
      uint32_t linkname_len;
      if (has_big_linkname) {
        has_big_linkname = false;
        linkname_ptr = str_dyn_linkname.ptr;
        linkname_len = (uint32_t)str_dyn_linkname.len;
      } else {
        linkname_ptr = header->linkname;
        linkname_len = small_linkname_len;
      }
      if (header->isextended) {
        // TODO
        BREAKPOINT();
      }

      {
        // Get file mode/permissions (rwxrwxrwx)
        uint32_t mode;
        if (parse_octal_integer32(&mode, header->mode, ARRAY_COUNT(header->mode)) != 0) {
          fprintf(stderr, "Bad file mode '%.*s'\n", pathname_len, pathname_ptr);
          return 1;
        }

        print_tar_entry(entry_type, 0, pathname_ptr, pathname_len, linkname_ptr, linkname_len, mode);
      }
    } break;
    case E_entry_type__gnu_L: {
      has_big_pathname = true;
      uint32_t pathname_size;
      if (parse_octal_integer32(&pathname_size, header->size, ARRAY_COUNT(header->size)) != 0) {
        fprintf(stderr, "Bad file size '%.*s'\n", (int)small_pathname_len, header->pathname);
        return 1;
      }

      // TODO Overflow checking ?
      uint32_t records_for_pathname = (pathname_size + TAR_RECORD_SIZE_IN_BYTES - 1) / TAR_RECORD_SIZE_IN_BYTES;
      str_dyn_pathname.len = 0;
      string_dynamic_required_unused_capacity(&str_dyn_pathname, records_for_pathname * TAR_RECORD_SIZE_IN_BYTES);
      size_t records_read = read_records(&str_dyn_pathname.ptr[0], records_for_pathname, stream);
      if (records_read != records_for_pathname) {
        fprintf(stderr, "File is truncated\n");
        return 1;
      }
      str_dyn_pathname.len = my_strnlen(str_dyn_pathname.ptr, records_read * TAR_RECORD_SIZE_IN_BYTES);
    } break;
    case E_entry_type__gnu_K: {
      has_big_linkname = true;
      uint32_t linkname_size;
      if (parse_octal_integer32(&linkname_size, header->size, ARRAY_COUNT(header->size)) != 0) {
        fprintf(stderr, "Bad file size '%.*s'\n", (int)small_pathname_len, header->pathname);
        return 1;
      }

      // TODO Overflow checking ?
      uint32_t records_for_linkname = (linkname_size + TAR_RECORD_SIZE_IN_BYTES - 1) / TAR_RECORD_SIZE_IN_BYTES;
      str_dyn_linkname.len = 0;
      string_dynamic_required_unused_capacity(&str_dyn_linkname, records_for_linkname * TAR_RECORD_SIZE_IN_BYTES);
      size_t records_read = read_records(&str_dyn_linkname.ptr[0], records_for_linkname, stream);
      if (records_read != records_for_linkname) {
        fprintf(stderr, "File is truncated\n");
        return 1;
      }
      str_dyn_linkname.len = my_strnlen(str_dyn_linkname.ptr, records_read * TAR_RECORD_SIZE_IN_BYTES);
    } break;
    }
  }

  if (last_record_was_zero) {
    if (found_two_zero_records) {
      // printf("Found two zero records\n");
    } else {
      printf("Found only one zero record\n");
    }
  } else {
    printf("Did not find any zero records\n");
  }
  return 0;
}
